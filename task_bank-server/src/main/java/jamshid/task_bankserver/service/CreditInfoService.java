package jamshid.task_bankserver.service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import jamshid.task_bankserver.entity.CreditInfo;
import jamshid.task_bankserver.entity.PassportInfo;
import jamshid.task_bankserver.payload.CheckCredit;
import jamshid.task_bankserver.payload.MyApiResponse;
import jamshid.task_bankserver.payload.dto.CreditReqDto;
import jamshid.task_bankserver.repository.CreditRepository;
import jamshid.task_bankserver.utills.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.URL;
import java.net.URLConnection;

@Service
public class CreditInfoService {
    @Autowired
    CreditRepository creditRepository;

    public MyApiResponse creditRequest(CreditReqDto dto) {
        Gson gson = new Gson();
        Type type = new TypeToken<PassportInfo>(){}.getType();
        try {
            URL url = new URL("http://localhost:8080/api/passportInfo/get/"+dto.getPassportSeries()+"/"+dto.getPassportNumber());
            URLConnection connection = url.openConnection();
            InputStreamReader inputStream = new InputStreamReader(connection.getInputStream());
            PassportInfo info = gson.fromJson(inputStream, type);
            CreditInfo creditInfo = makeCreditInfo(dto, info);
            creditRepository.save(creditInfo);
            CheckCredit checkCredit = CheckCredit.checkCredit(creditInfo.getSalary(), AppConstants.CREDIT_PERCENT, creditInfo.getCreditReqSum());
            if (checkCredit.isSuccess()){
                return new MyApiResponse(
                        true,
                        "Success");
            }else {
                return new MyApiResponse(true, "Salary is not enough", Math.round(checkCredit.getSuggestedCreditSum()));
            }

        } catch (Exception e) {
            e.printStackTrace();
            return new MyApiResponse(false, "Passport info not found");
        }
    }

    public CreditInfo makeCreditInfo(CreditReqDto creditReqDto, PassportInfo passportInfo){
        CreditInfo creditInfo = new CreditInfo();

        creditInfo.setCreditReqSum(creditReqDto.getCreditReqSum());
        creditInfo.setSalary(creditReqDto.getSalary());
        creditInfo.setPassportNumber(creditReqDto.getPassportNumber());
        creditInfo.setPassportSeries(creditReqDto.getPassportSeries());

        creditInfo.setFirstName(passportInfo.getFirstName());
        creditInfo.setLastName(passportInfo.getLastName());
        creditInfo.setFathersName(passportInfo.getFathersName());
        creditInfo.setDateOfBirth(passportInfo.getDateOfBirth());
        creditInfo.setPlaceOfBirth(passportInfo.getPlaceOfBirth());
        creditInfo.setDateOfExpire(passportInfo.getDateOfExpire());
        creditInfo.setDateOfIssue(passportInfo.getDateOfIssue());
        creditInfo.setJins(passportInfo.getJins());
        creditInfo.setNationality(passportInfo.getNationality());

        return creditInfo;
    }


}

