package jamshid.task_bankserver.service;

import jamshid.task_bankserver.entity.PassportInfo;
import jamshid.task_bankserver.payload.MyApiResponse;
import jamshid.task_bankserver.payload.dto.PassportInfoDto;
import jamshid.task_bankserver.repository.PassportInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PassportInfoService {
    @Autowired
    PassportInfoRepository passportInfoRepository;

    public MyApiResponse add(PassportInfoDto dto) {
        try {
            passportInfoRepository.save(dtoToPassInfo(dto));
            return new MyApiResponse(true, "Successfully saved");
        } catch (Exception e) {
            e.printStackTrace();
            return new MyApiResponse(false, "Not saved");
        }
    }


    public MyApiResponse getInfo(String series, Integer number) {
        try {
            PassportInfo passportInfo = passportInfoRepository.findByPassportSeriesAndPassportNumber(series, number).orElseThrow(() ->
                    new IllegalStateException("Info not found"));
            return new MyApiResponse(true, "Success", passInfoToDto(passportInfo));
        } catch (Exception e) {
            e.printStackTrace();
            return new MyApiResponse(false, "Info not found");
        }
    }

    public PassportInfoDto passInfoToDto(PassportInfo passportInfo) {
        PassportInfoDto dto = new PassportInfoDto();
        dto.setDateOfBirth(passportInfo.getDateOfBirth());
        dto.setDateOfExpire(passportInfo.getDateOfExpire());
        dto.setDateOfIssue(passportInfo.getDateOfIssue());
        dto.setFathersName(passportInfo.getFathersName());
        dto.setFirstName(passportInfo.getFirstName());
        dto.setJins(passportInfo.getJins());
        dto.setLastName(passportInfo.getLastName());
        dto.setNationality(passportInfo.getNationality());
        dto.setPassportNumber(passportInfo.getPassportNumber());
        dto.setPassportSeries(passportInfo.getPassportSeries());
        dto.setPlaceOfBirth(passportInfo.getPlaceOfBirth());
        return dto;
    }

    public PassportInfo dtoToPassInfo(PassportInfoDto dto) {
        PassportInfo passportInfo = new PassportInfo();
        passportInfo.setDateOfBirth(dto.getDateOfBirth());
        passportInfo.setDateOfExpire(dto.getDateOfExpire());
        passportInfo.setDateOfIssue(dto.getDateOfIssue());
        passportInfo.setFathersName(dto.getFathersName());
        passportInfo.setFirstName(dto.getFirstName());
        passportInfo.setJins(dto.getJins());
        passportInfo.setLastName(dto.getLastName());
        passportInfo.setNationality(dto.getNationality());
        passportInfo.setPlaceOfBirth(dto.getPlaceOfBirth());
        passportInfo.setPassportSeries(dto.getPassportSeries());
        passportInfo.setPassportNumber(dto.getPassportNumber());
        return passportInfo;
    }
}

