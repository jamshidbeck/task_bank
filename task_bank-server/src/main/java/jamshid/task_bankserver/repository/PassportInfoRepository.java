package jamshid.task_bankserver.repository;

import jamshid.task_bankserver.entity.PassportInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PassportInfoRepository extends JpaRepository<PassportInfo, Long> {
    Optional<PassportInfo> findByPassportSeriesAndPassportNumber(String series, Integer number);
}
