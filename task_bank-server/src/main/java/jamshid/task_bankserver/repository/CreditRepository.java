package jamshid.task_bankserver.repository;

import jamshid.task_bankserver.entity.CreditInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CreditRepository extends JpaRepository<CreditInfo, Long> {
}
