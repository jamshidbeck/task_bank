package jamshid.task_bankserver.controller;

import jamshid.task_bankserver.payload.MyApiResponse;
import jamshid.task_bankserver.payload.dto.CreditReqDto;
import jamshid.task_bankserver.payload.dto.PassportInfoDto;
import jamshid.task_bankserver.service.CreditInfoService;
import jamshid.task_bankserver.service.PassportInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/credit")
public class CreditInfoController {
    @Autowired
    CreditInfoService creditInfoService;

    @PostMapping("/get")
    public HttpEntity<?> get(@RequestBody CreditReqDto dto){
        MyApiResponse addResponse = creditInfoService.creditRequest(dto);
        return ResponseEntity.status(addResponse.isSuccess()?200:409).body(addResponse);
    }

}
