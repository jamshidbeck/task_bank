package jamshid.task_bankserver.controller;

import jamshid.task_bankserver.payload.MyApiResponse;
import jamshid.task_bankserver.payload.dto.PassportInfoDto;
import jamshid.task_bankserver.service.PassportInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/passportInfo")
public class PassportInfoController {
    @Autowired
    PassportInfoService passportInfoService;

    @PostMapping("/add")
    public HttpEntity<?> add(@RequestBody PassportInfoDto dto){
        MyApiResponse addResponse = passportInfoService.add(dto);
        return ResponseEntity.status(addResponse.isSuccess()?201:409).body(addResponse);
    }
    @GetMapping("/get/{series}/{number}")
    public HttpEntity<?> getInfo(@PathVariable String series, @PathVariable Integer  number){
        MyApiResponse addResponse = passportInfoService.getInfo(series, number);
        return ResponseEntity.status(addResponse.isSuccess()?200:409).body(addResponse);
    }
}
