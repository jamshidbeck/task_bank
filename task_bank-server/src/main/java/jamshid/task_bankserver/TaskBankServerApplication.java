package jamshid.task_bankserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskBankServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(TaskBankServerApplication.class, args);
    }

}
