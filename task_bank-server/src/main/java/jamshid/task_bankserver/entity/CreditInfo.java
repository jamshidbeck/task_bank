package jamshid.task_bankserver.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import jamshid.task_bankserver.entity.base.BaseEntity;
import jamshid.task_bankserver.entity.enums.Jins;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"passportSeries", "passportNumber"})
})
public class CreditInfo extends BaseEntity {
    private String passportSeries;
    private int passportNumber;
    private String firstName;
    private String lastName;
    private String fathersName;

    private String placeOfBirth;

    @JsonFormat(pattern = "dd.MM.yyyy")
    private Date dateOfBirth;

    @JsonFormat(pattern = "dd.MM.yyyy")
    private Date dateOfIssue;

    @JsonFormat(pattern = "dd.MM.yyyy")
    private Date dateOfExpire;

    private String nationality;

    @Enumerated(EnumType.STRING)
    private Jins jins;

    @Column(nullable = false)
    private double salary;

    @Column(nullable = false)
    private double creditReqSum;

    private double creditResSum;
}


