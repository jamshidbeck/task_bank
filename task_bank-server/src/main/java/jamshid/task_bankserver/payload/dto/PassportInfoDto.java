package jamshid.task_bankserver.payload.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import jamshid.task_bankserver.entity.enums.Jins;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class PassportInfoDto {
    private String passportSeries;
    private int passportNumber;
    private String firstName;
    private String lastName;
    private String fathersName;
    private String placeOfBirth;
    private Date dateOfBirth;
    private Date dateOfIssue;
    private Date dateOfExpire;
    private String nationality;
    private Jins jins;
}
