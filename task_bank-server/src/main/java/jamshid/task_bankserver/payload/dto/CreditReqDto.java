package jamshid.task_bankserver.payload.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreditReqDto {
    private String passportSeries;
    private int passportNumber;
    private double salary;
    private double creditReqSum;
}
