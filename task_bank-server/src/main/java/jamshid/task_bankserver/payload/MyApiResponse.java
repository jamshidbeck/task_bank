package jamshid.task_bankserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MyApiResponse {
    private boolean success;
    private String message;
    private Object data;
    private double suggestedCreditSum;

    public MyApiResponse(boolean success, String message, Object data) {
        this.success = success;
        this.message = message;
        this.data = data;
    }

    public MyApiResponse(boolean success, String message, double suggestedCreditSum) {
        this.success = success;
        this.message = message;
        this.suggestedCreditSum = suggestedCreditSum;
    }

    public MyApiResponse(boolean success, String message) {
        this.success = success;
        this.message = message;
    }
}