package jamshid.task_bankserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CheckCredit {
    private boolean success;
    private double suggestedCreditSum;


    public static CheckCredit checkCredit(double salary, double percentage, double creditReqSum) {
        double allSalary = salary * 12 * 0.7;
        double creditSumWithPercent = creditReqSum * (1 + percentage / 100);
        if (allSalary >= creditSumWithPercent) {
            return new CheckCredit(true, creditReqSum);
        }
        return new CheckCredit(false, allSalary / (1 + percentage / 100));


    }
}
